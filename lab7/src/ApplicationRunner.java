import javax.swing.*;
import java.awt.*;

/**
 * Created by vlad on 16.12.16.
 */
public class ApplicationRunner {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setSize(1300, 500);
        Dimension d = frame.getSize();
        d.height = d.width/2;
        d.width = d.height/2;
        frame.setLayout(new GridLayout(1, 2));
        ImageEditor editor = new ImageEditor(d);
        frame.add(editor);
        frame.add(new SourceImage(editor.getSourceImage(), d));
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
