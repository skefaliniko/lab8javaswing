import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class GetHistogram extends JPanel {

    Dimension dimensionOfPicture;

    BufferedImage picture;

    String colorReceived;

    int [] samples;

    boolean isRead;
    boolean isGreen;
    boolean isBlue;

    double firstMoment = 0;
    double secondMoment = 0;
    double dispersion = 0;
    double standartDeviation = 0; //sqrt(sigma)

    public GetHistogram(Dimension d, String color, File image)
    {
        try {
            dimensionOfPicture = d;

            colorReceived = color;

            picture = ImageIO.read(image);

        } catch (IOException ex) {
            Logger.getLogger(GetHistogram.class.getName()).log(Level.SEVERE, null, ex);
        }
        getHistogram();
    }

    public void getHistogram()
    {
        samples = new int[256];
        int maxNumSamples = 0;

        for(int i=0; i < 255; i++)
        {
            samples[i] = 0;
        }

        for(int width = 0; width < ( picture.getWidth()); width++ )
        {
            for(int height = 0; height < ( picture.getHeight()); height++ )
            {
                int redColor =  (0xff &(picture.getRGB(width, height)  >> 16));
                int greenColor = (0xff &(picture.getRGB(width, height)  >> 8));
                int blueColor =  (0xff & picture.getRGB(width, height));

                if(colorReceived.equals("red"))
                {
                    samples[redColor]++;
                    if(samples[redColor] > maxNumSamples)
                    {
                        maxNumSamples = samples[redColor];
                        isRead = true;
                        isBlue = false;
                        isGreen = false;
                    }

                }
                if(colorReceived.equals("green"))
                {
                    samples[greenColor]++;
                    if(samples[greenColor] > maxNumSamples)
                    {
                        maxNumSamples = samples[greenColor];
                        isRead = false;
                        isBlue = false;
                        isGreen = true;
                    }
                }
                if(colorReceived.equals("blue"))
                {
                    samples[blueColor]++;
                    if(samples[blueColor] > maxNumSamples)
                    {
                        maxNumSamples = samples[blueColor];
                        isRead = false;
                        isBlue = true;
                        isGreen = false;
                    }
                }
            }
        }

        for(int i = 0; i < 255; i++)
        {
            samples[i] = (int)((samples[i]*200)/(maxNumSamples));
        }
    }

    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);

        if(colorReceived.equals("red"))
        {
            g.setColor(Color.RED);
        }
        if (colorReceived.equals("green"))
        {
            g.setColor(Color.GREEN);
        }
        if (colorReceived.equals("blue"))
        {
            g.setColor(Color.BLUE);
        }


        double tempSum = 0;


        for(int i = 0; i < 255; i++)
        {
            double tempValue = samples[i];
            tempSum += tempValue;
            firstMoment += (i * tempValue);
            secondMoment += (i * i * tempValue);

            g.drawLine(i, 0,  i,  samples[i]);
        }

        if (isRead) {
            System.out.println("For red color");
        }
        if (isGreen) {
            System.out.println("For green color");
        }
        if (isBlue) {
            System.out.println("For blue color");
        }

        firstMoment /= tempSum;
        secondMoment /= tempSum;
        dispersion = secondMoment - firstMoment * firstMoment;
        standartDeviation = Math.sqrt(Math.abs(dispersion));

        System.out.println("Начальный момент 1го порядка или мат.ожидание");
        System.out.println(firstMoment + "\n");
        System.out.println("Начальный момент 2го порядка");
        System.out.println(secondMoment + "\n");
        System.out.println("Центральный момент 2го порядка или дисперсия");
        System.out.println(dispersion + "\n");
        System.out.println("Среднеквадратичное отклонение");
        System.out.println(standartDeviation + "\n");
    }
}
