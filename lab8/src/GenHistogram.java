import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import javax.swing.*;

public class GenHistogram extends JFrame implements WindowListener {

    public GenHistogram()
    {
        super("Image and histograms. Lab_8))");
    }

    public  void run() {
        String filename = JOptionPane.showInputDialog("Enter filename:",
                "/home/vlad/Downloads/s.jpg");

        File pic = new File(filename);

        JPanel panel = new JPanel();
        add(panel, BorderLayout.EAST);
        panel.setLayout(new GridLayout(3,1));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(800,600);
        setVisible(true);

        Dimension d = getSize();
        d.height = d.width/2;
        d.width = d.height/2;
        PaintPicture imagePicture = new PaintPicture(d,pic);

        GetHistogram redHistogramPicture = new GetHistogram(d,"red", pic);
        GetHistogram greenHistogramPicture = new GetHistogram(d,"green",pic);
        GetHistogram blueHistogramPicture = new GetHistogram(d,"blue",pic);

        add(imagePicture);

        panel.setPreferredSize(new Dimension(300, 800));
        panel.add(redHistogramPicture);
        panel.add(greenHistogramPicture);
        panel.add(blueHistogramPicture);

        validate();
    }

    public static void main(String[] args) {
        GenHistogram histogram = new GenHistogram();
        histogram.run();
    }

    ////////////////////////////////////////
    public void windowActivated(WindowEvent e) {}
    public void windowDeactivated(WindowEvent e) {}
    public void windowOpened(WindowEvent e) {}
    public void windowClosed(WindowEvent e) {}
    public void windowClosing(WindowEvent e) {System.exit(0);}
    public void windowIconified(WindowEvent e) {}
    public void windowDeiconified(WindowEvent e) {}

}
